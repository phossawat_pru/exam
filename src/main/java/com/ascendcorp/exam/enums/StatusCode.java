package com.ascendcorp.exam.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum StatusCode {
    APPROVED_200("200", "approved"),
    GENERAL_INVALID_DATA_400("400", "General Invalid Data"),
    GENERAL_TRANSACTION_ERROR_500("500", "General Transaction Error"),
    GENERAL_INVALID_DATA_501("501", "General Invalid Data"),
    ERROR_TIMEOUT_503("503", "Error timeout"),
    INTERNAL_APPLICATION_ERROR_504("504", "Internal Application Error");

    private final String code;
    private final String desc;
}
