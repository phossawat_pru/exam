package com.ascendcorp.exam.service;

import com.ascendcorp.exam.enums.StatusCode;
import com.ascendcorp.exam.model.Inquiry;
import com.ascendcorp.exam.model.InquiryServiceResultDTO;
import com.ascendcorp.exam.model.TransferResponse;
import com.ascendcorp.exam.proxy.BankProxyGateway;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.xml.ws.WebServiceException;

@Slf4j
public class InquiryService {

    private static final String APPROVED = "approved";
    private static final String INVALID_DATA = "invalid_data";
    private static final String TRANSACTION_ERROR = "transaction_error";
    private static final String UNKNOWN = "unknown";


    @Autowired
    private BankProxyGateway bankProxyGateway;

    public InquiryServiceResultDTO inquiry(Inquiry inquiry) {
        InquiryServiceResultDTO respDTO = null;
        try {
            log.info("validate request parameters.");
            validateRequestParams(inquiry);

            log.info("call bank web service");
            TransferResponse response = bankProxyGateway.requestTransfer(
                    inquiry.getTransactionId(),
                    inquiry.getTranDateTime(),
                    inquiry.getChannel(),
                    inquiry.getBankCode(),
                    inquiry.getBankNumber(),
                    inquiry.getAmount(),
                    inquiry.getReference1(),
                    inquiry.getReference2());

            log.info("check bank response code");
            if (response != null) //New
            {
                log.debug("found response code");
                respDTO = new InquiryServiceResultDTO();
                respDTO.setRefNo1(response.getReferenceCode1());
                respDTO.setRefNo2(response.getReferenceCode2());
                respDTO.setAmount(response.getAmount());
                respDTO.setTranID(response.getBankTransactionID());

                checkBankResponseCode(respDTO, response);
            } else
                // no response from bank
                throw new Exception("Unable to inquiry from service.");
        } catch (NullPointerException ne) {
            respDTO = respError(respDTO, StatusCode.GENERAL_INVALID_DATA_501);
        } catch (WebServiceException r) {
            // handle error from bank web service
            String faultString = r.getMessage();
            if (faultString != null && (faultString.indexOf("java.net.SocketTimeoutException") > -1
                    || faultString.indexOf("Connection timed out") > -1)) {
                // bank timeout
                respDTO = respError(respDTO, StatusCode.ERROR_TIMEOUT_503);
            } else {
                // bank general error
                respDTO = respError(respDTO, StatusCode.INTERNAL_APPLICATION_ERROR_504);
            }
        } catch (Exception e) {
            log.error("inquiry exception", e);
            respDTO = respError(respDTO, StatusCode.INTERNAL_APPLICATION_ERROR_504);
        }
        return respDTO;
    }

    private void validateRequestParams(Inquiry inquiry) {
        if (inquiry.getTransactionId() == null) {
            log.info("Transaction id is required!");
            throw new NullPointerException("Transaction id is required!");
        }
        if (inquiry.getTranDateTime() == null) {
            log.info("Transaction DateTime is required!");
            throw new NullPointerException("Transaction DateTime is required!");
        }
        if (inquiry.getChannel() == null) {
            log.info("Channel is required!");
            throw new NullPointerException("Channel is required!");
        }
        if (inquiry.getBankCode() == null || inquiry.getBankCode().equalsIgnoreCase("")) {
            log.info("Bank Code is required!");
            throw new NullPointerException("Bank Code is required!");
        }
        if (inquiry.getBankNumber() == null || inquiry.getBankNumber().equalsIgnoreCase("")) {
            log.info("Bank Number is required!");
            throw new NullPointerException("Bank Number is required!");
        }
        if (inquiry.getAmount() <= 0) {
            log.info("Amount must more than zero!");
            throw new NullPointerException("Amount must more than zero!");
        }
    }

    private void checkBankResponseCode(InquiryServiceResultDTO respDTO, TransferResponse response) throws Exception {
        switch (response.getResponseCode()) {
            case APPROVED:
                approved(respDTO, response.getDescription());
                break;
            case INVALID_DATA:
                invalidData(respDTO, response.getDescription());
                break;
            case TRANSACTION_ERROR:
                transactionError(respDTO, response.getDescription());
                break;
            case UNKNOWN:
                unknown(respDTO, response.getDescription());
                break;
            default:
                // bank code not support
                throw new Exception("Unsupport Error Reason Code");

        }
    }

    // bank response code = approved
    private void approved(InquiryServiceResultDTO respDTO, String description) {
        respDTO.setReasonCode(StatusCode.APPROVED_200.getCode());
        respDTO.setReasonDesc(description);
        respDTO.setAccountName(description);
    }

    // bank response code = invalid_data
    private void invalidData(InquiryServiceResultDTO respDTO, String description) {
        if (description != null) {
            String[] respDesc = description.split(":");
            if (respDesc != null && respDesc.length >= 3) {
                // bank description full format
                respDTO.setReasonCode(respDesc[1]);
                respDTO.setReasonDesc(respDesc[2]);
            } else {
                // bank description short format
                respDTO.setReasonCode(StatusCode.GENERAL_INVALID_DATA_400.getCode());
                respDTO.setReasonDesc(StatusCode.GENERAL_INVALID_DATA_400.getDesc());
            }
        } else {
            // bank no description
            respDTO.setReasonCode(StatusCode.GENERAL_INVALID_DATA_400.getCode());
            respDTO.setReasonDesc(StatusCode.GENERAL_INVALID_DATA_400.getDesc());
        }
    }

    // bank response code = transaction_error
    private void transactionError(InquiryServiceResultDTO respDTO, String description) {
        if (description != null) {
            String[] respDesc = description.split(":");
            if (respDesc != null && respDesc.length >= 2) {
                log.info("Case Inquiry Error Code Format Now Will Get From [0] and [1] first");
                String subIdx1 = respDesc[0];
                String subIdx2 = respDesc[1];
                log.info("index[0] : " + subIdx1 + " index[1] is >> " + subIdx2);
                if ("98".equalsIgnoreCase(subIdx1)) {
                    // bank code 98
                    respDTO.setReasonCode(subIdx1);
                    respDTO.setReasonDesc(subIdx2);
                } else {
                    log.info("case error is not 98 code");
                    if (respDesc.length >= 3) {
                        // bank description full format
                        String subIdx3 = respDesc[2];
                        log.info("index[0] : " + subIdx3);
                        respDTO.setReasonCode(subIdx2);
                        respDTO.setReasonDesc(subIdx3);
                    } else {
                        // bank description short format
                        respDTO.setReasonCode(subIdx1);
                        respDTO.setReasonDesc(subIdx2);
                    }
                }
            } else {
                // bank description incorrect format
                respDTO.setReasonCode(StatusCode.GENERAL_TRANSACTION_ERROR_500.getCode());
                respDTO.setReasonDesc(StatusCode.GENERAL_TRANSACTION_ERROR_500.getDesc());
            }
        } else {
            // bank no description
            respDTO.setReasonCode(StatusCode.GENERAL_TRANSACTION_ERROR_500.getCode());
            respDTO.setReasonDesc(StatusCode.GENERAL_TRANSACTION_ERROR_500.getDesc());
        }
    }

    // bank response code = unknown
    private void unknown(InquiryServiceResultDTO respDTO, String description) {
        if (description != null) {
            String[] respDesc = description.split(":");
            if (respDesc != null && respDesc.length >= 2) {
                // bank description full format
                respDTO.setReasonCode(respDesc[0]);
                respDTO.setReasonDesc(respDesc[1]);
                if (respDTO.getReasonDesc() == null || respDTO.getReasonDesc().trim().length() == 0) {
                    respDTO.setReasonDesc(StatusCode.GENERAL_INVALID_DATA_501.getDesc());
                }
            } else {
                // bank description short format
                respDTO.setReasonCode(StatusCode.GENERAL_INVALID_DATA_501.getCode());
                respDTO.setReasonDesc(StatusCode.GENERAL_INVALID_DATA_501.getDesc());
            }
        } else {
            // bank no description
            respDTO.setReasonCode(StatusCode.GENERAL_INVALID_DATA_501.getCode());
            respDTO.setReasonDesc(StatusCode.GENERAL_INVALID_DATA_501.getDesc());
        }
    }

    private InquiryServiceResultDTO respError(InquiryServiceResultDTO respDTO, StatusCode statusCode) {
        if (respDTO == null || respDTO.getReasonCode() == null) {
            respDTO = new InquiryServiceResultDTO();
            respDTO.setReasonCode(statusCode.getCode());
            respDTO.setReasonDesc(statusCode.getDesc());
        }
        return respDTO;
    }
}
