package com.ascendcorp.exam.proxy;

import com.ascendcorp.exam.model.TransferResponse;

import java.time.LocalDateTime;

public class BankProxyGateway {

    public TransferResponse requestTransfer(
            String transactionId, LocalDateTime tranDateTime, String channel,
            String bankCode, String bankNumber, double amount,
            String reference1, String reference2) {

        return new TransferResponse();
    }
}

