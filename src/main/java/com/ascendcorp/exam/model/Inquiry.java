package com.ascendcorp.exam.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class Inquiry {

    private String transactionId;

    private LocalDateTime tranDateTime;

    private String channel;

    private String locationCode;

    private String bankCode;

    private String bankNumber;

    private double amount;

    private String reference1;

    private String reference2;

    private String firstName;

    private String lastName;

}
