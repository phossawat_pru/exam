package com.ascendcorp.exam.service;

import com.ascendcorp.exam.enums.StatusCode;
import com.ascendcorp.exam.model.Inquiry;
import com.ascendcorp.exam.model.InquiryServiceResultDTO;
import com.ascendcorp.exam.model.TransferResponse;
import com.ascendcorp.exam.proxy.BankProxyGateway;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.xml.ws.WebServiceException;
import java.sql.SQLException;
import java.time.LocalDateTime;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class InquiryServiceTest {

    @InjectMocks
    InquiryService inquiryService;

    @Mock
    BankProxyGateway bankProxyGateway;

    @Test
    public void should_return501_when_noRequireValue() throws SQLException {

        // Transaction Id
        InquiryServiceResultDTO inquiry = inquiryService.inquiry(new Inquiry(null, LocalDateTime.now(),
                "Mobile", null,
                "BANK1", "4321000", 100d, "rrivsffv234c",
                "11223xfgt", null, null));

        assertNotNull(inquiry);
        assertEquals(StatusCode.GENERAL_INVALID_DATA_501.getCode(), inquiry.getReasonCode());
        assertEquals(StatusCode.GENERAL_INVALID_DATA_501.getDesc(), inquiry.getReasonDesc());

        // Datetime
        inquiry = inquiryService.inquiry(new Inquiry("1234", null,
                "Mobile", null,
                "BANK1", "4321000", 100d, "rrivsffv234c",
                "11223xfgt", null, null));

        assertNotNull(inquiry);
        assertEquals(StatusCode.GENERAL_INVALID_DATA_501.getCode(), inquiry.getReasonCode());
        assertEquals(StatusCode.GENERAL_INVALID_DATA_501.getDesc(), inquiry.getReasonDesc());

        // Channel
        inquiry = inquiryService.inquiry(new Inquiry("1234", LocalDateTime.now(),
                null, null,
                "BANK1", "4321000", 100d, "rrivsffv234c",
                "11223xfgt", null, null));

        assertNotNull(inquiry);
        assertEquals(StatusCode.GENERAL_INVALID_DATA_501.getCode(), inquiry.getReasonCode());
        assertEquals(StatusCode.GENERAL_INVALID_DATA_501.getDesc(), inquiry.getReasonDesc());

        // BankCode
        inquiry = inquiryService.inquiry(new Inquiry("1234", LocalDateTime.now(),
                "Mobile", null,
                null, "4321000", 100d, "rrivsffv234c",
                "11223xfgt", null, null));

        assertNotNull(inquiry);
        assertEquals(StatusCode.GENERAL_INVALID_DATA_501.getCode(), inquiry.getReasonCode());
        assertEquals(StatusCode.GENERAL_INVALID_DATA_501.getDesc(), inquiry.getReasonDesc());

        // BankNumber
        inquiry = inquiryService.inquiry(new Inquiry("1234", LocalDateTime.now(),
                "Mobile", null,
                "BANK1", null, 100d, "rrivsffv234c",
                "11223xfgt", null, null));

        assertNotNull(inquiry);
        assertEquals(StatusCode.GENERAL_INVALID_DATA_501.getCode(), inquiry.getReasonCode());
        assertEquals(StatusCode.GENERAL_INVALID_DATA_501.getDesc(), inquiry.getReasonDesc());

        // Amount
        inquiry = inquiryService.inquiry(new Inquiry("1234", LocalDateTime.now(),
                "Mobile", null,
                "BANK1", "4321000", 0d, "rrivsffv234c",
                "11223xfgt", null, null));

        assertNotNull(inquiry);
        assertEquals(StatusCode.GENERAL_INVALID_DATA_501.getCode(), inquiry.getReasonCode());
        assertEquals(StatusCode.GENERAL_INVALID_DATA_501.getDesc(), inquiry.getReasonDesc());
    }


    @Test
    public void should_return200_when_bankApproved() throws SQLException {
        TransferResponse transferResponse = new TransferResponse();
        transferResponse.setResponseCode("approved");
        transferResponse.setDescription("approved");

        when(bankProxyGateway.requestTransfer(anyString(), any(), anyString(), anyString(), anyString(),
                anyDouble(), anyString(), anyString())).thenReturn(transferResponse);


        InquiryServiceResultDTO inquiry = inquiryService.inquiry(new Inquiry("123456", LocalDateTime.now(),
                "Mobile", null,
                "BANK1", "4321000", 100d, "rrivsffv234c",
                "11223xfgt", null, null));

        assertNotNull(inquiry);
        assertEquals(StatusCode.APPROVED_200.getCode(), inquiry.getReasonCode());
        assertEquals(StatusCode.APPROVED_200.getDesc(), inquiry.getReasonDesc());
    }

    @Test
    public void should_return400_when_invalidDataWithoutDesc() throws SQLException {
        TransferResponse transferResponse = new TransferResponse();
        transferResponse.setResponseCode("invalid_data");

        when(bankProxyGateway.requestTransfer(anyString(), any(), anyString(), anyString(), anyString(),
                anyDouble(), anyString(), anyString())).thenReturn(transferResponse);


        InquiryServiceResultDTO inquiry = inquiryService.inquiry(new Inquiry("123456", LocalDateTime.now(),
                "Mobile", null,
                "BANK1", "4321000", 100d, "rrivsffv234c",
                "11223xfgt", null, null));

        assertNotNull(inquiry);
        assertEquals(StatusCode.GENERAL_INVALID_DATA_400.getCode(), inquiry.getReasonCode());
        assertEquals(StatusCode.GENERAL_INVALID_DATA_400.getDesc(), inquiry.getReasonDesc());
    }


    @Test
    public void should_return1091WithReasonDesc_when_invalidDataWithDescAndCode() throws SQLException {
        TransferResponse transferResponse = new TransferResponse();
        transferResponse.setResponseCode("invalid_data");
        transferResponse.setDescription("100:1091:Data type is invalid.");

        when(bankProxyGateway.requestTransfer(anyString(), any(), anyString(), anyString(), anyString(),
                anyDouble(), anyString(), anyString())).thenReturn(transferResponse);


        InquiryServiceResultDTO inquiry = inquiryService.inquiry(new Inquiry("123456", LocalDateTime.now(),
                "Mobile", null,
                "BANK1", "4321000", 100d, "rrivsffv234c",
                "11223xfgt", null, null));

        assertNotNull(inquiry);
        assertEquals("1091", inquiry.getReasonCode());
        assertEquals("Data type is invalid.", inquiry.getReasonDesc());
    }

    @Test
    public void should_return400_when_invalidDataWithDesc() throws SQLException {
        TransferResponse transferResponse = new TransferResponse();
        transferResponse.setResponseCode("invalid_data");
        transferResponse.setDescription("General error.");

        when(bankProxyGateway.requestTransfer(anyString(), any(), anyString(), anyString(), anyString(),
                anyDouble(), anyString(), anyString())).thenReturn(transferResponse);


        InquiryServiceResultDTO inquiry = inquiryService.inquiry(new Inquiry("123456", LocalDateTime.now(),
                "Mobile", null,
                "BANK1", "4321000", 100d, "rrivsffv234c",
                "11223xfgt", null, null));

        assertNotNull(inquiry);
        assertEquals(StatusCode.GENERAL_INVALID_DATA_400.getCode(), inquiry.getReasonCode());
        assertEquals(StatusCode.GENERAL_INVALID_DATA_400.getDesc(), inquiry.getReasonDesc());
    }

    @Test
    public void should_return500_when_errorAndDescIsNull() throws SQLException {
        TransferResponse transferResponse = new TransferResponse();
        transferResponse.setResponseCode("transaction_error");


        when(bankProxyGateway.requestTransfer(anyString(), any(), anyString(), anyString(), anyString(),
                anyDouble(), anyString(), anyString())).thenReturn(transferResponse);


        InquiryServiceResultDTO inquiry = inquiryService.inquiry(new Inquiry("123456", LocalDateTime.now(),
                "Mobile", null,
                "BANK1", "4321000", 100d, "rrivsffv234c",
                "11223xfgt", null, null));

        assertNotNull(inquiry);
        assertEquals(StatusCode.GENERAL_TRANSACTION_ERROR_500.getCode(), inquiry.getReasonCode());
        assertEquals(StatusCode.GENERAL_TRANSACTION_ERROR_500.getDesc(), inquiry.getReasonDesc());
    }

    @Test
    public void should_return500_when_errorAndNoDescCode() throws SQLException {
        TransferResponse transferResponse = new TransferResponse();
        transferResponse.setResponseCode("transaction_error");
        transferResponse.setDescription("Transaction error.");

        when(bankProxyGateway.requestTransfer(anyString(), any(), anyString(), anyString(), anyString(),
                anyDouble(), anyString(), anyString())).thenReturn(transferResponse);

        InquiryServiceResultDTO inquiry = inquiryService.inquiry(new Inquiry("123456", LocalDateTime.now(),
                "Mobile", null,
                "BANK1", "4321000", 100d, "rrivsffv234c",
                "11223xfgt", null, null));

        assertNotNull(inquiry);
        assertEquals(StatusCode.GENERAL_TRANSACTION_ERROR_500.getCode(), inquiry.getReasonCode());
        assertEquals(StatusCode.GENERAL_TRANSACTION_ERROR_500.getDesc(), inquiry.getReasonDesc());
    }

    @Test
    public void should_return1091_when_errorAndDesc3Code() throws SQLException {
        TransferResponse transferResponse = new TransferResponse();
        transferResponse.setResponseCode("transaction_error");
        transferResponse.setDescription("100:1091:Transaction is error with code 1091.");

        when(bankProxyGateway.requestTransfer(anyString(), any(), anyString(), anyString(), anyString(),
                anyDouble(), anyString(), anyString())).thenReturn(transferResponse);

        InquiryServiceResultDTO inquiry = inquiryService.inquiry(new Inquiry("123456", LocalDateTime.now(),
                "Mobile", null,
                "BANK1", "4321000", 100d, "rrivsffv234c",
                "11223xfgt", null, null));

        assertNotNull(inquiry);
        assertEquals("1091", inquiry.getReasonCode());
        assertEquals("Transaction is error with code 1091.", inquiry.getReasonDesc());
    }

    @Test
    public void should_return1092_when_errorAndDesc2Code() throws SQLException {
        TransferResponse transferResponse = new TransferResponse();
        transferResponse.setResponseCode("transaction_error");
        transferResponse.setDescription("1092:Transaction is error with code 1092.");

        when(bankProxyGateway.requestTransfer(anyString(), any(), anyString(), anyString(), anyString(),
                anyDouble(), anyString(), anyString())).thenReturn(transferResponse);

        InquiryServiceResultDTO inquiry = inquiryService.inquiry(new Inquiry("123456", LocalDateTime.now(),
                "Mobile", null,
                "BANK1", "4321000", 100d, "rrivsffv234c",
                "11223xfgt", null, null));

        assertNotNull(inquiry);
        assertEquals("1092", inquiry.getReasonCode());
        assertEquals("Transaction is error with code 1092.", inquiry.getReasonDesc());
    }

    @Test
    public void should_return98_when_errorAndDescCode98() throws SQLException {
        TransferResponse transferResponse = new TransferResponse();
        transferResponse.setResponseCode("transaction_error");
        transferResponse.setDescription("98:Transaction is error with code 98.");

        when(bankProxyGateway.requestTransfer(anyString(), any(), anyString(), anyString(), anyString(),
                anyDouble(), anyString(), anyString())).thenReturn(transferResponse);

        InquiryServiceResultDTO inquiry = inquiryService.inquiry(new Inquiry("123456", LocalDateTime.now(),
                "Mobile", null,
                "BANK1", "4321000", 100d, "rrivsffv234c",
                "11223xfgt", null, null));

        assertNotNull(inquiry);
        assertEquals("98", inquiry.getReasonCode());
        assertEquals("Transaction is error with code 98.", inquiry.getReasonDesc());
    }

    @Test
    public void should_return501_when_unknownAndWithoutDesc() throws SQLException {
        TransferResponse transferResponse = new TransferResponse();
        transferResponse.setResponseCode("unknown");

        when(bankProxyGateway.requestTransfer(anyString(), any(), anyString(), anyString(), anyString(),
                anyDouble(), anyString(), anyString())).thenReturn(transferResponse);

        InquiryServiceResultDTO inquiry = inquiryService.inquiry(new Inquiry("123456", LocalDateTime.now(),
                "Mobile", null,
                "BANK1", "4321000", 100d, "rrivsffv234c",
                "11223xfgt", null, null));

        assertNotNull(inquiry);
        assertEquals(StatusCode.GENERAL_INVALID_DATA_501.getCode(), inquiry.getReasonCode());
        assertEquals(StatusCode.GENERAL_INVALID_DATA_501.getDesc(), inquiry.getReasonDesc());
    }

    @Test
    public void should_return5001_when_unknownAndDesc() throws SQLException {
        TransferResponse transferResponse = new TransferResponse();
        transferResponse.setResponseCode("unknown");
        transferResponse.setDescription("5001:Unknown error code 5001");

        when(bankProxyGateway.requestTransfer(anyString(), any(), anyString(), anyString(), anyString(),
                anyDouble(), anyString(), anyString())).thenReturn(transferResponse);

        InquiryServiceResultDTO inquiry = inquiryService.inquiry(new Inquiry("123456", LocalDateTime.now(),
                "Mobile", null,
                "BANK1", "4321000", 100d, "rrivsffv234c",
                "11223xfgt", null, null));

        assertNotNull(inquiry);
        assertEquals("5001", inquiry.getReasonCode());
        assertEquals("Unknown error code 5001", inquiry.getReasonDesc());
    }

    @Test
    public void should_return5002_when_unknownAndEmptyDesc() throws SQLException {
        TransferResponse transferResponse = new TransferResponse();
        transferResponse.setResponseCode("unknown");
        transferResponse.setDescription("5002: ");

        when(bankProxyGateway.requestTransfer(anyString(), any(), anyString(), anyString(), anyString(),
                anyDouble(), anyString(), anyString())).thenReturn(transferResponse);

        InquiryServiceResultDTO inquiry = inquiryService.inquiry(new Inquiry("123456", LocalDateTime.now(),
                "Mobile", null,
                "BANK1", "4321000", 100d, "rrivsffv234c",
                "11223xfgt", null, null));

        assertNotNull(inquiry);
        assertEquals("5002", inquiry.getReasonCode());
        assertEquals("General Invalid Data", inquiry.getReasonDesc());
    }

    @Test
    public void should_return501_when_unknownAndTextDesc() throws SQLException {
        TransferResponse transferResponse = new TransferResponse();
        transferResponse.setResponseCode("unknown");
        transferResponse.setDescription("General Invalid Data code 501");

        when(bankProxyGateway.requestTransfer(anyString(), any(), anyString(), anyString(), anyString(),
                anyDouble(), anyString(), anyString())).thenReturn(transferResponse);

        InquiryServiceResultDTO inquiry = inquiryService.inquiry(new Inquiry("123456", LocalDateTime.now(),
                "Mobile", null,
                "BANK1", "4321000", 100d, "rrivsffv234c",
                "11223xfgt", null, null));

        assertNotNull(inquiry);
        assertEquals(StatusCode.GENERAL_INVALID_DATA_501.getCode(), inquiry.getReasonCode());
        assertEquals(StatusCode.GENERAL_INVALID_DATA_501.getDesc(), inquiry.getReasonDesc());
    }

    @Test
    public void should_return504_when_errorDescNotSupport() throws SQLException {
        TransferResponse transferResponse = new TransferResponse();
        transferResponse.setResponseCode("not_support");
        transferResponse.setDescription("Not support");

        when(bankProxyGateway.requestTransfer(anyString(), any(), anyString(), anyString(), anyString(),
                anyDouble(), anyString(), anyString())).thenReturn(transferResponse);

        InquiryServiceResultDTO inquiry = inquiryService.inquiry(new Inquiry("123456", LocalDateTime.now(),
                "Mobile", null,
                "BANK1", "4321000", 100d, "rrivsffv234c",
                "11223xfgt", null, null));

        assertNotNull(inquiry);
        assertEquals(StatusCode.INTERNAL_APPLICATION_ERROR_504.getCode(), inquiry.getReasonCode());
        assertEquals(StatusCode.INTERNAL_APPLICATION_ERROR_504.getDesc(), inquiry.getReasonDesc());
    }

    @Test
    public void should_return504_when_responseNull() throws SQLException {

        when(bankProxyGateway.requestTransfer(anyString(), any(), anyString(), anyString(), anyString(),
                anyDouble(), anyString(), anyString())).thenReturn(null);

        InquiryServiceResultDTO inquiry = inquiryService.inquiry(new Inquiry("123456", LocalDateTime.now(),
                "Mobile", null,
                "BANK1", "4321000", 100d, "rrivsffv234c",
                "11223xfgt", null, null));

        assertNotNull(inquiry);
        assertEquals(StatusCode.INTERNAL_APPLICATION_ERROR_504.getCode(), inquiry.getReasonCode());
        assertEquals(StatusCode.INTERNAL_APPLICATION_ERROR_504.getDesc(), inquiry.getReasonDesc());
    }

    @Test
    public void should_return504_when_throwWebServiceException() throws SQLException {

        when(bankProxyGateway.requestTransfer(anyString(), any(), anyString(), anyString(), anyString(),
                anyDouble(), anyString(), anyString())).thenThrow(WebServiceException.class);

        InquiryServiceResultDTO inquiry = inquiryService.inquiry(new Inquiry("123456", LocalDateTime.now(),
                "Mobile", null,
                "BANK1", "4321000", 100d, "rrivsffv234c",
                "11223xfgt", null, null));

        assertNotNull(inquiry);
        assertEquals(StatusCode.INTERNAL_APPLICATION_ERROR_504.getCode(), inquiry.getReasonCode());
        assertEquals(StatusCode.INTERNAL_APPLICATION_ERROR_504.getDesc(), inquiry.getReasonDesc());
    }

    @Test
    public void should_return503_when_socketTimeout() throws SQLException {

        WebServiceException ex = new WebServiceException("java.net.SocketTimeoutException error");

        when(bankProxyGateway.requestTransfer(anyString(), any(), anyString(), anyString(), anyString(),
                anyDouble(), anyString(), anyString())).thenThrow(ex);

        InquiryServiceResultDTO inquiry = inquiryService.inquiry(new Inquiry("123456", LocalDateTime.now(),
                "Mobile", null,
                "BANK1", "4321000", 100d, "rrivsffv234c",
                "11223xfgt", null, null));

        assertNotNull(inquiry);
        assertEquals(StatusCode.ERROR_TIMEOUT_503.getCode(), inquiry.getReasonCode());
        assertEquals(StatusCode.ERROR_TIMEOUT_503.getDesc(), inquiry.getReasonDesc());
    }

    @Test
    public void should_return503_when_connectionTimeout() throws SQLException {

        WebServiceException ex = new WebServiceException("Server Connection timed out");

        when(bankProxyGateway.requestTransfer(anyString(), any(), anyString(), anyString(), anyString(),
                anyDouble(), anyString(), anyString())).thenThrow(ex);

        InquiryServiceResultDTO inquiry = inquiryService.inquiry(new Inquiry("123456", LocalDateTime.now(),
                "Mobile", null,
                "BANK1", "4321000", 100d, "rrivsffv234c",
                "11223xfgt", null, null));

        assertNotNull(inquiry);
        assertEquals(StatusCode.ERROR_TIMEOUT_503.getCode(), inquiry.getReasonCode());
        assertEquals(StatusCode.ERROR_TIMEOUT_503.getDesc(), inquiry.getReasonDesc());
    }
}
